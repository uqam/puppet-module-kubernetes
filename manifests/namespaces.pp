# Resource: kubernetes::namespaces
# ===========================
#
# Configure namespaces and quotas on a kubernetes instance
#
# Parameters
# ----------
#
# * `namespaces (Hash[Object])`
#  Hash containing namespaces to create.
#  Example:
#     namespaces         => {
#       'mynamespace1' => {
#         ensure => 'present',
#         quota_hard_cpu => '200m',
#         quota_hard_memory => '2Gi',
#         labels => {
#           'team' => 'infra',
#           'app'  => 'test'
#         },
#         annotations => {
#           'created' => 'now',
#           'comment' => 'useful comment',
#         }
#       },
#     }
#
# * `autoload_path (String)`
#  String containing path where to create resources yaml files
#  Example: /etc/kubernetes/autoload
#
# Examples
# --------
#
# @example
#    class { 'kubernetes::master':
#      namespaces         => {
#       'mynamespace1' => {
#         ensure => 'present',
#         quota_hard_cpu => '200m',
#         quota_hard_memory => '2Gi',
#         labels => {
#           'team' => 'infra',
#           'app'  => 'test'
#         },
#         annotations => {
#           'created' => 'now',
#           'comment' => 'useful comment',
#         }
#       },
#       'namespace2' => {
#         ensure => 'present',
#         quota_hard_cpu => '200m',
#         quota_hard_memory => '2Gi',
#         labels => {
#           'team' => 'infra',
#           'app'  => 'test'
#         },
#       },
#      },
#      autoload_path      => '/etc/kubernetes/autoload',
#    }
#
# @example in hiera data:
#
# kubernetes::namespaces:
#   mynamespace:
#     ensure: 'present'
#     quota_hard_pods: '4'
#     quota_hard_limits_cpu: '2'
#     quota_hard_limits_memory: 1Gi
#     quota_hard_cpu: 200m
#     quota_hard_memory: 512Mi
#     labels:
#       team: infra
#       app: test
#     annotations:
#       responsable: Rémi Croset
#
# Authors
# -------
#
# Rémi Croset <croset.remi@uqam.ca>
#
# Copyright
# ---------
#
# Copyright 2018 Rémi Croset.
#
class kubernetes::namespaces (
  Hash             $namespaces    = $kubernetes::params::namespaces,
  Optional[String] $autoload_path = $kubernetes::params::autoload_path,
  ) inherits kubernetes::params {

  $defaults = {
    'ensure'        => 'present',
    'autoload_path' => $kubernetes::params::autoload_path
  }
  create_resources(kubernetes::namespace,$namespaces, $defaults)
}

# Defined resource to create namespaces
define kubernetes::namespace (
  String           $ensure                             = 'present',
  Optional[String] $quota_hard_pods                    = $kubernetes::params::quota_hard_pods,
  Optional[String] $quota_hard_limits_cpu              = $kubernetes::params::quota_hard_limits_cpu,
  Optional[String] $quota_hard_limits_memory           = $kubernetes::params::quota_hard_limits_memory,
  Optional[String] $quota_hard_cpu                     = $kubernetes::params::quota_hard_cpu,
  Optional[String] $quota_hard_memory                  = $kubernetes::params::quota_hard_memory,
  Optional[String] $limit_container_default_cpu        = $kubernetes::params::limit_container_default_cpu,
  Optional[String] $limit_container_default_memory     = $kubernetes::params::limit_container_default_memory,
  Optional[String] $limit_container_default_cpu_req    = $kubernetes::params::limit_container_default_cpu_req,
  Optional[String] $limit_container_default_memory_req = $kubernetes::params::limit_container_default_memory_req,
  Optional[String] $autoload_path                      = $kubernetes::params::autoload_path,
  Optional[Hash]   $labels                             = undef,
  Optional[Hash]   $annotations                        = undef,
  ){

  include kubernetes

  if $ensure == 'present' {
    $present_status = 'present'
    $absent_status  = 'absent'
  } elsif $ensure == 'absent' {
    $present_status = 'absent'
    $absent_status  = 'present'
  }

  # Create namespaces yaml files
  file { "${autoload_path}/present/ns-${name}.yaml":
    ensure  => $present_status,
    content => template('kubernetes/namespace.yaml.erb'),
  }

  file { "${autoload_path}/absent/ns-${name}.yaml":
    ensure  => $absent_status,
    content => template('kubernetes/namespace.yaml.erb'),
  }
}
