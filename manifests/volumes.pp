# Resource: kubernetes::rolebindings
# ===========================
#
# Configure volumes.
#
# Parameters
# ----------
#
# * `volumes (Hash[Object])`
#  Hash containing rolebindings to create.
#
# * `autoload_path (String)`
#  String containing path where to create resources yaml files
#  Example: /etc/kubernetes/autoload
#
# Examples
# --------
#
# @example
#    class { 'kubernetes::volumes':
#      volumes         => {
#        'vol01' => {
#          ensure => 'present',
#          kind   => 'User',
#          namespaces => {
#            'namespace-dev-01' => {
#              'role'   => 'edit',
#              'labels' => {
#                'team' => 'infra',
#                'app'  => 'test',
#              },
#              'annotations' => {
#                'responsable' => 'John Doe',
#              },
#            },
#          },
#        },
#      },
#      autoload_path      => '/etc/kubernetes/autoload',
#    }
#
# @example in hiera data:
#
# kubernetes::volumes:
#   my-volume-name:
#     ensure: 'present'
#     accessmode: ReadWriteOnce
#     capacity: 1Gi
#     reclaimpolicy: Retain
#     nfspath: /nfs/path/vol01
#     nfsserver: nfsserver.example.com
#     namespace: mynamespace
#
# Authors
# -------
#
# Rémi Croset <croset.remi@uqam.ca>
#
# Copyright
# ---------
#
# Copyright 2018 Rémi Croset.
#
class kubernetes::volumes (
  Hash              $volumes               = $kubernetes::params::volumes,
  Optional[String]  $autoload_path         = $kubernetes::params::autoload_path,
  ) inherits kubernetes::params {

  $defaults = {
    'ensure'        => 'present',
    'reclaimpolicy' => 'Retain',
    'autoload_path' => $kubernetes::params::autoload_path
  }
  create_resources(kubernetes::volume,$volumes, $defaults)
}

# Defined resource to create persistentvolumes and persistentvolumeclaims
define kubernetes::volume (
  String           $ensure        = 'present',
  String           $accessmode    = undef,
  String           $capacity      = undef,
  String           $namespace     = undef,
  String           $reclaimpolicy = undef,
  Optional[String] $nfsserver     = undef,
  Optional[String] $nfspath       = undef,
  Optional[String] $autoload_path,
  ){

  include kubernetes

  if $ensure == 'present' {
    $present_status = 'present'
    $absent_status  = 'absent'
  } elsif $ensure == 'absent' {
    $present_status = 'absent'
    $absent_status  = 'present'
  }

  # Create namespaces yaml files
  file { "${autoload_path}/present/pv-${name}.yaml":
    ensure  => $present_status,
    content => template('kubernetes/volume.yaml.erb'),
  }

  file { "${autoload_path}/absent/pv-${name}.yaml":
    ensure  => $absent_status,
    content => template('kubernetes/volume.yaml.erb'),
  }
}
