# Class: kubernetes
# ===========================
#
# Set the environment and dependencies to be ready to install kubernetes with kubeadm on Centos 7.
# If you have issues with kubeadm, checkout requirements in the official documentation: https://kubernetes.io/docs/setup/independent/install-kubeadm/
#
# Parameters
# ----------
#
# * `api_server`
#  fqdn of your master
#
# * `etcd_endpoints`
#  Array containing URLs of all the nodes of the etcd cluster.
#  Example: etcd_endpoints = ['https://etcd-1.domain.tld:2379', 'https://etcd-2.domain.tld:2379', 'https://etcd-3.domain.tld:2379']
#
# * `api_port - Optional`
# Default is 6443 (kubeadm's default value)
#
# * `scheduler_port - Optional`
#  Integer containing scheduler port to open in the firewall.
#  It is not yet used to configure actual scheduler port binding.
#  If you want to do so you need to edit the file kubeadm.conf.erb
#  Check the official documentation here: https://kubernetes.io/docs/reference/setup-tools/kubeadm/kubeadm-init/ #Using kubeadm init with a configuration file
#
# * `controller_manager_port - Optional`
#  Integer containing controller manager port to open in the firewall.
#  It is not yet used to configure actual controller manager port binding.
#  If you want to do so you need to edit the file kubeadm.conf.erb
#  Check the official documentation here: https://kubernetes.io/docs/reference/setup-tools/kubeadm/kubeadm-init/ #Using kubeadm init with a configuration file
#
# * `weave_ports - Optional`
#  Array of Integer containing weave ports to open in the firewall.
#  It is not yet used to configure actual weave ports binding.
#  If you want to do so you need to edit the file kubeadm.conf.erb
#  Check the official documentation here: https://kubernetes.io/docs/reference/setup-tools/kubeadm/kubeadm-init/ #Using kubeadm init with a configuration file
#
# * `cluster_pod_subnet - Optional`
# Pods subnet, default is 10.96.0.0/12
#
# * `kubernetes_version - Optional`
# Tell kubeadm what version of kubernetes deploy, default is 1.10.2
#
# Variables
# ----------
#
# * `manage_firewall`
#  Default: true
#  Set to false if you don't wan't firewall rules to be added
#
# * `manage_selinux`
#  Default: true
#  Set to false if you don't wan't selinux to be set to permissive (required by kubeadm)
#
# Examples
# --------
#
# @example
#    class { 'kubernetes':
#      api_server              => 'master.domain.tld',
#      etcd_endpoints          => ['https://etcd-1.domain.tld:2379','https://etcd-2.domain.tld:2379','https://etcd-3.domain.tld:2379'],
#      api_port                => 6443,
#      scheduler_port          => 10251,
#      controller_manager_port => 10252,
#      weave_ports             => [ 6781, 6782, 6783 ],
#      cluster_pod_subnet      => '10.96.0.0/12',
#      kubernetes_version      => '1.10.2',
#      manage_firewall         => true,
#      manage_selinux          => true,
#    }
#
# Authors
# -------
#
# Rémi Croset <croset.remi@uqam.ca>
#
# Copyright
# ---------
#
# Copyright 2018 Rémi Croset.
#
class kubernetes (
  String                   $api_server              = $kubernetes::params::api_server,
  String                   $api_server_ip           = $kubernetes::params::api_server_ip,
  Array[String]            $api_server_cert_sans    = $kubernetes::params::api_server_cert_sans,
  Array[String]            $etcd_endpoints          = $kubernetes::params::etcd_endpoints,
  Optional[Integer]        $api_port                = $kubernetes::params::api_port,
  Optional[Integer]        $scheduler_port          = $kubernetes::params::scheduler_port,
  Optional[Integer]        $controller_manager_port = $kubernetes::params::controller_manager_port,
  Optional[Array[Integer]] $weave_ports             = $kubernetes::params::weave_ports,
  Optional[String]         $cluster_pod_subnet      = $kubernetes::params::cluster_pod_subnet,
  Optional[String]         $cluster_dns             = $kubernetes::params::cluster_dns,
  Optional[String]         $kubernetes_version      = $kubernetes::params::kubernetes_version,
  Optional[Boolean]        $manage_firewall         = $kubernetes::params::manage_firewall,
  Optional[Boolean]        $manage_selinux          = $kubernetes::params::manage_selinux,
  Optional[String]         $docker_logs_max_size    = $kubernetes::params::docker_logs_max_size,
  Optional[Integer]        $docker_logs_max_files   = $kubernetes::params::docker_logs_max_files,
  ) inherits kubernetes::params {

  if $manage_selinux {
    include selinux
  }

  yumrepo { 'kubernetes':
    descr    => 'Kubernetes',
    baseurl  => 'https://packages.cloud.google.com/yum/repos/kubernetes-el7-x86_64',
    gpgkey   => 'https://packages.cloud.google.com/yum/doc/rpm-package-key.gpg',
    gpgcheck => true,
  }

  package { ['docker','etcd']:
    ensure  => 'present',
    require => Yumrepo['kubernetes'],
  }

  package { ['kubeadm','kubelet','kubectl']:
    ensure  => $kubernetes_version,
    require => Yumrepo['kubernetes'],
  }

  sysctl { 'net.bridge.bridge-nf-call-iptables': value => '1' }
  sysctl { 'net.bridge.bridge-nf-call-ip6tables': value => '1' }

  service { 'docker':
    ensure  => 'running',
    enable  => true,
    require => Package['docker'],
  }

  service { 'kubelet':
    ensure  => 'running',
    enable  => true,
    require => Package['kubelet'],
  }

  file { '/root/kubeadm_config.yaml':
    ensure  => 'present',
    content => template('kubernetes/kubernetes/kubeadm_config.yaml.erb'),
    owner   => 'root',
    group   => 'root',
    mode    => '0644',
  }

  file { '/root/.kube':
    ensure => directory,
  }

  file { '/root/.kube/config':
    ensure => 'link',
    target => '/etc/kubernetes/admin.conf',
  }

  if $manage_firewall {
    firewall { '100 Weave':
      dport  => $weave_ports,
      proto  => 'tcp',
      action => 'accept',
    }
  }

  file { '/etc/systemd/system/docker-clean.service':
    ensure => 'present',
    source => 'puppet:///modules/kubernetes/docker-clean.service',
    owner  => 'root',
    group  => 'root',
    mode   => '0644',
  } ~> Class['kubernetes::daemon_reload']

  file { '/etc/systemd/system/docker-clean.timer':
    ensure => 'present',
    source => 'puppet:///modules/kubernetes/docker-clean.timer',
    owner  => 'root',
    group  => 'root',
    mode   => '0644',
  } ~> Class['kubernetes::daemon_reload']

  service { 'docker-clean.timer':
    ensure    => 'running',
    enable    => true,
    subscribe => File['/etc/systemd/system/docker-clean.timer'],
    require   => [File['/etc/systemd/system/docker-clean.service','/etc/systemd/system/docker-clean.timer']],
  }

  # Docker drop-in to limit log size
  file { '/etc/systemd/system/docker.service.d':
    ensure => 'directory',
    owner  => 'root',
    group  => 'root',
    mode   => '0644',
  }
  file { '/etc/systemd/system/docker.service.d/10-docker-logs.conf':
    ensure => 'present',
    content => template('kubernetes/docker/10-docker-logs.conf.erb'),
    owner  => 'root',
    group  => 'root',
    mode   => '0644',
    require => File['/etc/systemd/system/docker.service.d'],
  } ~> Class['kubernetes::daemon_reload']
}

# Fix kubelet drop-in to avoid the error:
# Failed to get system container stats for "/system.slice/kubelet.service":
# failed to get cgroup stats for "/system.slice/kubelet.service":
# failed to get container info for "/system.slice/kubelet.service": unknown container "/system.slice/kubelet.service"

class kubernetes::fix_kubelet {
  exec { "sed -i \"s#KUBELET_EXTRA_ARGS=.*#KUBELET_EXTRA_ARGS=\\\"--cgroup-driver=systemd --runtime-cgroups=/systemd/system.slice --kubelet-cgroups=/systemd/system.slice\\\"#\" /etc/sysconfig/kubelet":
    unless => 'grep runtime-cgroups /etc/sysconfig/kubelet',
    path   => '/usr/bin',
  }
}
