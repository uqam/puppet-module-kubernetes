# Resource: kubernetes::kubeconfigs
# ===========================
#
# Generate user kubeconfigs for connecting with certificates
# Do not forget to create the rolebinding
#
# Parameters
# ----------
#
# * `namespaces (Hash[Object])`
#  Hash containing namespaces to create.
#  Example:
#     kubeconfigs         => {
#       'account1' => {
#         ensure => 'present',
#       },
#       'account2' => {
#         ensure => 'present',
#       },
#     }
#
#
# Examples
# --------
#
# @example
#    class { 'kubernetes::kubeconfigs':
#      kubeconfigs         => {
#       'mynamespace1' => {
#         ensure => 'present',
#       }
#    }
#
# @example in hiera data:
#
# kubernetes::kubeconfigs:
#   account1:
#     ensure: 'present'
#
# Authors
# -------
#
# Rémi Croset <croset.remi@uqam.ca>
#
# Copyright
# ---------
#
# Copyright 2018 Rémi Croset.
#
class kubernetes::kubeconfigs (
  Hash              $kubeconfigs          = $kubernetes::params::kubeconfigs,
  Integer           $api_port             = $kubernetes::params::api_port,
  ) inherits kubernetes::params {

  $defaults = {
    'ensure'        => 'present',
  }
  create_resources(kubernetes::kubeconfig, $kubeconfigs, $defaults)

  # Create the directory to push config files:
  file { '/root/kubeconfigs':
    ensure => 'directory',
  }
}

# Defined resource to create namespaces
define kubernetes::kubeconfig (
  String           $ensure         = 'present',
  ){
  include kubernetes

  if $ensure == 'present' {
    exec { "Creating ${name} kubeconfig file":
      command => "kubeadm alpha phase kubeconfig user --client-name ${name} > /root/kubeconfigs/kubeconfig_${name}",
      unless  => "test -f /root/kubeconfigs/kubeconfig_${name}",
      onlyif  => 'kubectl get nodes',
      path    => ['/usr/bin/kubeadm','/usr/bin'],
    }
  } elsif $ensure == 'absent' {
    # Create namespaces yaml files
    file { "/root/kubeconfigs/kubeconfig_${name}":
      ensure  => 'absent',
    }
  }
}
