# Class: kubernetes::daemon_reload
# ===========================
#
# Reload systemd configuration
#
# Examples
# --------
#
# @example
#    file { 'your.service': } ~> Class['kubernetes::daemon_reload']
#
# Authors
# -------
#
# Rémi Croset <croset.remi@uqam.ca>
#
# Copyright
# ---------
#
# Copyright 2018 Rémi Croset.
#
class kubernetes::daemon_reload {
  exec { 'systemctl-daemon-reload-k8s':
    command     => 'systemctl daemon-reload',
    refreshonly => true,
    path        => '/usr/bin/',
  }
}
