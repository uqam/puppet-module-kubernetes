# Resource: kubernetes::rolebindings
# ===========================
#
# Configure user clusterrolebinding and rolebindings to namespaces.
#
# Parameters
# ----------
#
# * `rolebindings (Hash[Object])`
#  Hash containing rolebindings to create.
#
# * `autoload_path (String)`
#  String containing path where to create resources yaml files
#  Example: /etc/kubernetes/autoload
#
# Examples
# --------
#
# @example
#    class { 'kubernetes::rolebindings':
#      rolebindings         => {
#        'rb1' => {
#          ensure => 'present',
#          kind   => 'User',
#          namespaces => {
#            'namespace-dev-01' => {
#              'role'   => 'edit',
#              'labels' => {
#                'team' => 'infra',
#                'app'  => 'test',
#              },
#              'annotations' => {
#                'responsable' => 'John Doe',
#              },
#            },
#          },
#        },
#      },
#      autoload_path      => '/etc/kubernetes/autoload',
#    }
#
# @example in hiera data:
#
# kubernetes::rolebindings:
#   kind: User
#   namespaces:
#     namespace-dev-01:
#       role: edit
#       labels:
#         team: infra
#         app: test
#       annotations:
#         responsable: John Doe
#     namespace-dev-02:
#       role: admin
#       labels:
#         team: infra
#         app: test
#       annotations:
#         responsable: John Odd
#
# Authors
# -------
#
# Rémi Croset <croset.remi@uqam.ca>
#
# Copyright
# ---------
#
# Copyright 2018 Rémi Croset.
#
class kubernetes::rolebindings (
  Hash              $rolebindings          = $kubernetes::params::rolebindings,
  Optional[Boolean] $authenticatedusers_ro = $kubernetes::params::authenticatedusers_ro,
  Optional[String]  $autoload_path         = $kubernetes::params::autoload_path,
  Optional[String]  $oidc_username_prefix  = $kubernetes::params::oidc_username_prefix,
  Optional[String]  $oidc_groups_prefix    = $kubernetes::params::oidc_groups_prefix,
  ) inherits kubernetes::params {

  $defaults = {
    'ensure'               => 'present',
    'autoload_path'        => $kubernetes::params::autoload_path,
    'oidc_username_prefix' => $kubernetes::params::oidc_username_prefix,
    'oidc_groups_prefix'   => $kubernetes::params::oidc_groups_prefix
  }
  create_resources(kubernetes::rolebinding,$rolebindings, $defaults)

  if $authenticatedusers_ro {
    $present_status = 'present'
    $absent_status  = 'absent'
  } else {
    $present_status = 'absent'
    $absent_status  = 'present'
  }

  # Create read only cluster role:
  file { "${autoload_path}/present/crb-authenticatedusers-ro.yaml":
    ensure => $present_status,
    source => 'puppet:///modules/kubernetes/crb-authenticatedusers-ro.yaml',
  }

  # Delete read only cluster role:
  file { "${autoload_path}/absent/crb-authenticatedusers-ro.yaml":
    ensure => $absent_status,
    source => 'puppet:///modules/kubernetes/crb-authenticatedusers-ro.yaml',
  }
}

# Defined resource to create rolebinding
define kubernetes::rolebinding (
  String           $ensure         = 'present',
  String           $kind           = 'User',
  Optional[Hash]   $namespaces     = undef,
  Optional[String] $autoload_path,
  Optional[String] $oidc_username_prefix,
  Optional[String] $oidc_groups_prefix,
  ){

  include kubernetes

  if $ensure == 'present' {
    $present_status = 'present'
    $absent_status  = 'absent'
  } elsif $ensure == 'absent' {
    $present_status = 'absent'
    $absent_status  = 'present'
  }

  $safename=regsubst($name, '(/| |\')', '_', 'G')

  # Create namespaces yaml files
  file { "${autoload_path}/present/rb-${safename}.yaml":
    ensure  => $present_status,
    content => template('kubernetes/rolebinding.yaml.erb'),
  }

  file { "${autoload_path}/absent/rb-${safename}.yaml":
    ensure  => $absent_status,
    content => template('kubernetes/rolebinding.yaml.erb'),
  }
}
