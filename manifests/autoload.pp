# Class: kubernetes::autoload
# ===========================
#
# Setup autoload service to create kubernetes resources based on yaml files.
#
# Parameters
# ----------
#
# * `kubeconfig (String) - Optional`
#  Path of the kubeconfig file.
#  Default: /etc/kubernetes/admin.conf
#  Example: kubeconfig => "/etc/kubernetes/admin.conf"
#
# * `autoload (String) - Optional`
#  Autoload directory.
#  Default: /etc/kubernetes/autoload
#  Example: autoload_path => '/etc/kubernetes/autoload'
#
# Examples
# --------
#
# @example
#    class { 'kubernetes::autoload':
#      kubeconfig    => '/etc/kubernetes/admin.conf',
#      autoload_path => '/etc/kubernetes/autoload'
#    }
#
# Authors
# -------
#
# Rémi Croset <croset.remi@uqam.ca>
#
# Copyright
# ---------
#
# Copyright 2018 Rémi Croset.
#
class kubernetes::autoload (
  Optional[String] $kubeconfig       = $kubernetes::params::kubeconfig,
  Optional[String] $autoload_path    = $kubernetes::params::autoload_path,
  Optional[String] $autoload_logpath = "${kubernetes::params::autoload_path}/autoload.log"
  ){

  include kubernetes::daemon_reload

  File {
    owner => 'root',
    group => 'root',
    mode  => '0644',
    backup => true,
  }

  file { '/etc/systemd/system/kubernetes-autoload.service':
    ensure  => 'present',
    content => template('kubernetes/autoload/kubernetes-autoload.service.erb'),
  } ~> Class['kubernetes::daemon_reload']

  file { '/etc/systemd/system/kubernetes-autoload.path':
    ensure  => 'present',
    content => template('kubernetes/autoload/kubernetes-autoload.path.erb'),
  } ~> Class['kubernetes::daemon_reload']

  service { 'kubernetes-autoload.service':
    subscribe => File['/etc/systemd/system/kubernetes-autoload.service']
  }

  service { 'kubernetes-autoload.path':
    ensure    => 'running',
    enable    => true,
    subscribe => File['/etc/systemd/system/kubernetes-autoload.path']
  }

  file { [ $autoload_path, "${autoload_path}/present", "${autoload_path}/absent", "${autoload_path}/backup" ]:
    ensure  => 'directory',
  }

  file { '/usr/local/bin/kubernetes-autoload':
    ensure  => 'present',
    mode    => '0533',
    content => template('kubernetes/autoload/kubernetes-autoload.sh.erb'),
  }
}
