# Class: kubernetes::worker
# ===========================
#
# Install a kubernetes node with kubeadm
# If you have issues with kubeadm, checkout the official documentation: https://kubernetes.io/docs/setup/independent/install-kubeadm/
#
# Parameters
# ----------
#
# * `api_server (String)`
#  FQDN of your master
#  Example: master.domain.tld
#
# * `api_port (Integer)`
#  Default is 6443 (kubeadm's default value)
#
# * `kubelet_port`
#  Integer containing kubelet port to open in the firewall.
#  Default value: 10250
#  It is not yet used to configure actual kubelet port binding.
#  If you want to do so you are welcome to help!
#
# * `kubelet_ro_port`
#  Integer containing kubelet read only port to open in the firewall.
#  Default value: 10255
#  It is not yet used to configure actual kubelet port binding.
#  If you want to do so you are welcome to help!
#
# * `etcd_endpoints (Arrray[String])`
#  Array containing endpoints URLs.
#  Example: ['https://etcd-1.domain.tld:2379','https://etcd-2.domain.tld:2379','https://etcd-3.domain.tld:2379']
#
# Variables
# ----------
#
# * `manage_firewall`
#  Default: true
#  Set to false if you don't wan't firewall rules to be added
#
# Examples
# --------
#
# @example
#    class { 'kubernetes::worker':
#      etcd_endpoints  => ['https://etcd-1.domain.tld:2379','https://etcd-2.domain.tld:2379','https://etcd-3.domain.tld:2379'],
#      nodeport_range  => '30000-31000',
#      kubelet_port    => 10250,
#      kubelet_ro_port => 10255,
#      manage_firewall => true,
#    }
#
# Authors
# -------
#
# Rémi Croset <croset.remi@uqam.ca>
#
# Copyright
# ---------
#
# Copyright 2018 Rémi Croset.
#
class kubernetes::worker (
  Array[String]     $etcd_endpoints  = $kubernetes::params::etcd_endpoints,
  Optional[String]  $nodeport_range  = $kubernetes::params::nodeport_range,
  Optional[Integer] $kubelet_port    = $kubernetes::params::kubelet_port,
  Optional[Integer] $kubelet_ro_port = $kubernetes::params::kubelet_ro_port,
  Optional[Boolean] $manage_firewall = $kubernetes::params::manage_firewall,
  ){

  include kubernetes
  include kubernetes::fix_kubelet
  include kubernetes::daemon_reload

  assert_type(Array[String, 1], $etcd_endpoints) |$expected, $actual| {
      fail("Error: Wrong type ${actual} for variable \$etcd_endpoints in module ${module_name}. Should be ${expected}.")
  }

  $etcd_endpoints_joined = join($etcd_endpoints, ',')

  # Pull kubeconfig from etcd
  exec { 'Pull kubeconfig etcd':
    command     => 'etcdctl get /uqam/admin --print-value-only | base64 -d > /etc/kubernetes/admin.conf',
    environment => ['ETCDCTL_API=3',
      'ETCDCTL_CACERT=/etc/puppetlabs/puppet/ssl/certs/ca.pem',
      "ETCDCTL_CERT=/etc/puppetlabs/puppet/ssl/certs/${::fqdn}.pem",
      "ETCDCTL_KEY=/etc/puppetlabs/puppet/ssl/private_keys/${::fqdn}.pem",
      "ETCDCTL_ENDPOINTS=${etcd_endpoints_joined}"],
    unless      => 'echo $(etcdctl get /uqam/admin --print-value-only | base64 -d | md5sum | cut -c -32) /etc/kubernetes/admin.conf |md5sum - --check --status',
    path        => '/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/opt/puppetlabs/bin:/root/bin',
    tries       => 12,
    try_sleep   => 10,
    require     => File['/etc/profile.d/etcdctl.sh'],
  }

  exec { 'Kubeadm join':
    command   => 'kubeadm alpha phase bootstraptoken create \
                  | grep -v bootstraptoken\
                  | awk \'{print $0 " --ignore-preflight-errors cri"}\' \
                  | bash',
    unless    => "kubectl --kubeconfig=/etc/kubernetes/admin.conf get nodes | grep -q \"${::hostname}.* Ready\"",
    path      => '/root/go/bin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/opt/puppetlabs/bin:/root/bin',
    tries     => 12,
    try_sleep => 10,
    require   => [
      File['/root/kubeadm_config.yaml'],
      Exec['Pull kubeconfig etcd'],
      Exec['Check Node status']
    ],
  } ~> Class['kubernetes::daemon_reload']

  exec { 'Check Node status':
    command     => 'kubeadm reset',
    onlyif      => "kubectl --kubeconfig=/etc/kubernetes/admin.conf get nodes ${::hostname} |& egrep -q \"No resources found|NotReady\"",
    path        => '/root/go/bin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/opt/puppetlabs/bin:/root/bin',
    refreshonly => true,
    tries       => 12,
    try_sleep   => 10,
    notify      => Exec['Kubeadm join'],
    require     => [
      File['/root/kubeadm_config.yaml'],
      Exec['Pull kubeconfig etcd']
    ],
  }

  if $manage_firewall {
    firewall { '100 Kubelet API':
      dport  => $kubelet_port,
      proto  => 'tcp',
      action => 'accept',
    }
    firewall { '100 Read-only Kubelet API':
      dport  => $kubelet_ro_port,
      proto  => 'tcp',
      action => 'accept',
    }
    firewall { '100 Nodeport Services':
      dport  => $nodeport_range,
      proto  => 'tcp',
      action => 'accept',
    }
  }
}
