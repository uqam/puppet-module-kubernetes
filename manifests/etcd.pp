# Class: kubernetes::etcd
# ===========================
#
# Set the environment and dependencies to be ready to bootstrap a new etcd cluster or join an existing one.
#
# Parameters
# ----------
#
# * `etcd_bootstrap_node`
#  URL of the node used to boostrap etcd.
#  Should be the one you associate with the class kubernetes::etcd::bootstrap
#  Example: https://etcd-1.domain.tld.2379
#
# * `etcd_endpoints`
#  Array containing URLs of all the nodes of the etcd cluster.
#  Example: etcd_endpoints = ['https://etcd-1.domain.tld:2379', 'https://etcd-2.domain.tld:2379', 'https://etcd-3.domain.tld:2379']
#
# * `etcd_ports`
#  Array containing etcd ports to open in the firewall.
#  It is not yet used to configure actual etcd port binding.
#  If you want to do so you need to edit the file etcd.conf.erb
#
# Variables
# ----------
#
# * `manage_firewall`
#  Default: true
#  Set to false if you don't wan't firewall rules to be added
#
# Examples
# --------
#
# @example
#    class { 'kubernetes::etcd':
#      etcd_bootstrap_node  => 'https://etcd-1.domain.tld:2379',
#      etcd_endpoints       => [ 'https://etcd-1.domain.tld:2379', 'https://etcd-2.domain.tld:2379', 'https://etcd-3.domain.tld:2379' ]
#      etcd_ports           => ['2379', '2380'],
#      manage_firewall      => true
#    }
#
#    class { 'kubernetes::etcd::bootstrap': }
#
#    class { 'kubernetes::etcd::join': }
#
# Authors
# -------
#
# Rémi Croset <croset.remi@uqam.ca>
#
# Copyright
# ---------
#
# Copyright 2018 Rémi Croset.
#
class kubernetes::etcd (
  String                    $etcd_bootstrap_node = $kubernetes::params::etcd_bootstrap_node,
  Array[String]             $etcd_endpoints      = $kubernetes::params::etcd_endpoints,
  Optional[Array[Integer]]  $etcd_ports          = $kubernetes::params::etcd_ports,
  Optional[Boolean]         $manage_firewall     = $kubernetes::params::manage_firewall,
  ){

  include kubernetes
  include kubernetes::daemon_reload

  # Todo: Put ETCDCTL_ENDPOINT in hiera variable
  file { '/etc/profile.d/etcdctl.sh':
    ensure  => 'present',
    content => inline_template("export ETCDCTL_API=3
export ETCDCTL_CACERT=\"/etc/puppetlabs/puppet/ssl/certs/ca.pem\"
export ETCDCTL_CERT=\"/etc/puppetlabs/puppet/ssl/certs/<%= @fqdn %>.pem\"
export ETCDCTL_KEY=\"/etc/puppetlabs/puppet/ssl/private_keys/<%= @fqdn %>.pem\"
export ETCDCTL_ENDPOINTS=\"<%= @etcd_endpoints.join(',') %>\""),
    owner   => 'root',
    group   => 'root',
    mode    => '0644',
  }

  file { '/etc/etcd/etcd.conf':
    ensure  => 'present',
    content => template('kubernetes/etcd/etcd.conf.erb'),
    owner   => 'root',
    group   => 'root',
    mode    => '0644',
  }

  service { 'etcd':
    ensure  => 'running',
    enable  => true,
    require => Package['etcd'],
  }

  user { 'etcd':
    groups => [ 'etcd', 'puppet'],
  }

  if $manage_firewall {
    firewall { '100 ETCD':
      dport  => $etcd_ports,
      proto  => 'tcp',
      action => 'accept',
    }
  }
}

# Bootstrap first etcd node
class kubernetes::etcd::bootstrap inherits kubernetes::etcd {

  if $manage_firewall {
    $bootstrap_require = [ File['/etc/etcd/etcd.conf'], File['/root/bootstrap_etcd.sh'], User['etcd'], Firewall['100 ETCD'] ]
  } else {
    $bootstrap_require = [ File['/etc/etcd/etcd.conf'], File['/root/bootstrap_etcd.sh'], User['etcd'] ]
  }

  file { '/root/bootstrap_etcd.sh':
    ensure  => 'present',
    content => template('kubernetes/etcd/bootstrap_etcd.sh.erb'),
    owner   => 'root',
    group   => 'root',
    mode    => '0644',
  }

  exec { 'Bootstrap ETCD':
    command => '/root/bootstrap_etcd.sh',
    path    => '/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/opt/puppetlabs/bin:/root/bin',
    unless  => '/usr/bin/test -d /var/lib/etcd/default.etcd',
    require => $bootstrap_require,
  }
}

# Class to Join ETCD cluster
class kubernetes::etcd::join inherits kubernetes::etcd {

  if $manage_firewall {
    $join_require = [ File['/etc/etcd/etcd.conf'], File['/root/join_etcd.sh'], User['etcd'], Firewall['100 ETCD'] ]
  } else {
    $join_require = [ File['/etc/etcd/etcd.conf'], File['/root/join_etcd.sh'], User['etcd'] ]
  }

  file { '/root/join_etcd.sh':
    ensure  => 'present',
    content => template('kubernetes/etcd/join_etcd.sh.erb'),
    owner   => 'root',
    group   => 'root',
    mode    => '0754',
  }

  exec { 'Join ETCD':
    command   => '/root/join_etcd.sh',
    path      => '/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/opt/puppetlabs/bin:/root/bin',
    unless    => '/usr/bin/test -d /var/lib/etcd/default.etcd',
    tries     => 12,
    try_sleep => 10,
    require   => $join_require,
  }
}

# Class to backup ETCD cluster
class kubernetes::etcd::backup (
  String $etcd_backup_path = $kubernetes::params::etcd_backup_path
  ) inherits kubernetes::etcd {

  include kubernetes::daemon_reload

  file { $etcd_backup_path:
    ensure => 'directory',
  }

  file { '/etc/systemd/system/etcd-backup.service':
    ensure  => 'present',
    content => template('kubernetes/etcd/etcd-backup.service.erb'),
    owner   => 'root',
    group   => 'root',
    mode    => '0644',
  } ~> Class['kubernetes::daemon_reload']

  file { '/etc/systemd/system/etcd-backup.timer':
    ensure => 'present',
    source => 'puppet:///modules/kubernetes/etcd/etcd-backup.timer',
    owner  => 'root',
    group  => 'root',
    mode   => '0644',
  } ~> Class['kubernetes::daemon_reload']

  service { 'etcd-backup.timer':
    ensure    => 'running',
    enable    => true,
    subscribe => File['/etc/systemd/system/etcd-backup.timer'],
    require   => [File['/etc/systemd/system/etcd-backup.service','/etc/systemd/system/etcd-backup.timer']],
  }

}
