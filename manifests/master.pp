# Class: kubernetes::master
# ===========================
#
# Install kubernetes master with kubeadm
# If you have issues with kubeadm, checkout the official documentation: https://kubernetes.io/docs/setup/independent/install-kubeadm/
#
# Parameters
# ----------
#
# * `api_server (String)`
#  FQDN of your master
#  Example: master.domain.tld
#
# * `api_port (Integer)`
#  Default is 6443 (kubeadm's default value)
#
# * `etcd_endpoints (Arrray[String])`
#  Array containing endpoints URLs.
#  Example: ['https://etcd-1.domain.tld:2379','https://etcd-2.domain.tld:2379','https://etcd-3.domain.tld:2379']
#
# Variables
# ----------
#
# * `manage_firewall`
#  Default: true
#  Set to false if you don't wan't firewall rules to be added
#
# * `manage_selinux`
#  Default: true
#  Set to false if you don't wan't selinux to be set to permissive (required by kubeadm)
#
# Examples
# --------
#
# @example
#    class { 'kubernetes::master':
#      api_server              => 'master.domain.tld',
#      etcd_endpoints          => ['https://etcd-1.domain.tld:2379','https://etcd-2.domain.tld:2379','https://etcd-3.domain.tld:2379'],
#      api_port                => 6443,
#      scheduler_port          => 10251,
#      controller_manager_port => 10252,
#      manage_firewall         => true,
#    }
#
# Authors
# -------
#
# Rémi Croset <croset.remi@uqam.ca>
#
# Copyright
# ---------
#
# Copyright 2018 Rémi Croset.
#
class kubernetes::master (
  String            $api_server              = $kubernetes::params::api_server,
  Array[String]     $etcd_endpoints          = $kubernetes::params::etcd_endpoints,
  Optional[Integer] $api_port                = $kubernetes::params::api_port,
  Optional[Integer] $scheduler_port          = $kubernetes::params::scheduler_port,
  Optional[Integer] $controller_manager_port = $kubernetes::params::controller_manager_port,
  Optional[Boolean] $manage_firewall         = $kubernetes::params::manage_firewall,
  Optional[String]  $oidc_issuer_url         = $kubernetes::params::oidc_issuer_url,
  Optional[String]  $oidc_client_id          = $kubernetes::params::oidc_client_id,
  Optional[String]  $oidc_username_claim     = $kubernetes::params::oidc_username_claim,
  Optional[String]  $oidc_username_prefix    = $kubernetes::params::oidc_username_prefix,
  Optional[String]  $oidc_groups_claim       = $kubernetes::params::oidc_groups_claim,
  Optional[String]  $oidc_groups_prefix      = $kubernetes::params::oidc_groups_prefix,
  ){

  include kubernetes
  include kubernetes::fix_kubelet
  include kubernetes::daemon_reload


  assert_type(Array[String, 1], $etcd_endpoints) |$expected, $actual| {
      fail("Error: Wrong type ${actual} for variable \$etcd_endpoints in module ${module_name}. Should be ${expected}.")
  }

  $etcd_endpoints_joined = join($etcd_endpoints, ',')

  exec { 'Kubeadm init':
    command   => 'kubeadm init --config /root/kubeadm_config.yaml',
    unless    => 'test -f /etc/kubernetes/manifests/kube-apiserver.yaml',
    path      => '/root/go/bin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/opt/puppetlabs/bin:/root/bin',
    tries     => 12,
    try_sleep => 10,
    require   => [
      File['/root/kubeadm_config.yaml'],
      Exec['Check ETCD'],
      Sysctl['net.bridge.bridge-nf-call-iptables']
      ],
  } ~> Class['kubernetes::daemon_reload']

  exec { 'Check ETCD':
    command     => 'etcdctl endpoint health | grep -q "is healthy" && touch /tmp/etcd_healthy',
    environment => ['ETCDCTL_API=3',
      'ETCDCTL_CACERT=/etc/puppetlabs/puppet/ssl/certs/ca.pem',
      "ETCDCTL_CERT=/etc/puppetlabs/puppet/ssl/certs/${::fqdn}.pem",
      "ETCDCTL_KEY=/etc/puppetlabs/puppet/ssl/private_keys/${::fqdn}.pem",
      "ETCDCTL_ENDPOINTS=${etcd_endpoints_joined}"],
    creates     => '/tmp/etcd_healthy',
    path        => '/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/opt/puppetlabs/bin:/root/bin',
    tries       => 12,
    try_sleep   => 10,
    refreshonly => true,
    require     => Exec['Bootstrap ETCD'],
  }

  exec { 'Install Weave':
    command => 'kubectl apply -f "https://cloud.weave.works/k8s/net?k8s-version=$(kubectl version | base64 | tr -d \'\n\')"',
    unless  => 'test $(docker ps --no-trunc | grep weave | wc -l) -gt 1',
    path    => '/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/opt/puppetlabs/bin:/root/bin',
    require => [ Exec['Check apiserver'], File['/root/.kube/config']]
  }

  exec { 'Check apiserver':
    command     => 'kubectl get nodes',
    path        => '/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/opt/puppetlabs/bin:/root/bin',
    refreshonly => true,
    tries       => 12,
    try_sleep   => 10,
    require     => Exec['Kubeadm init']
  }

  # Push kubeconfig etcd
  exec { 'Push kubeconfig etcd':
    command     => 'cat /etc/kubernetes/admin.conf |base64 | etcdctl put /uqam/admin',
    unless      => 'echo $(etcdctl get /uqam/admin --print-value-only | base64 -d | md5sum | cut -c -32) /etc/kubernetes/admin.conf |md5sum - --check --status',
    environment => ['ETCDCTL_API=3',
      'ETCDCTL_CACERT=/etc/puppetlabs/puppet/ssl/certs/ca.pem',
      "ETCDCTL_CERT=/etc/puppetlabs/puppet/ssl/certs/${::fqdn}.pem",
      "ETCDCTL_KEY=/etc/puppetlabs/puppet/ssl/private_keys/${::fqdn}.pem",
      "ETCDCTL_ENDPOINTS=${etcd_endpoints_joined}"],
    path        => '/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/opt/puppetlabs/bin:/root/bin',
    require     => Exec['Kubeadm init'],
  }

  # Firewall configuration
  if $manage_firewall {
    firewall { '100 API Server':
      dport  => $api_port,
      proto  => 'tcp',
      action => 'accept',
    }
    firewall { '100 kube-scheduler':
      dport  => $scheduler_port,
      proto  => 'tcp',
      action => 'accept',
    }
    firewall { '100 kube-controller-manager':
      dport  => $controller_manager_port,
      proto  => 'tcp',
      action => 'accept',
    }
  }
}
